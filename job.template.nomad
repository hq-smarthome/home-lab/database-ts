job "database-ts" {
  type = "service"
  region = "global"
  datacenters = ["proxima"]

  priority = 60

  vault {
    policies = ["job-database-ts"]
  }

  group "influx" {
    count = 1

    network {
      mode = "bridge"
    }

    volume "influx" {
      type = "host"
      read_only = false
      source = "influx"
    }

    restart {
      attempts = 10
      interval = "5m"
      delay = "25s"
      mode = "delay"
    }

    service {
      name = "influxdb"
      port = "8086"
      task = "influx"

      connect {
        sidecar_service {}
      }

      check {
        expose = true
        name = "Application Health Status"
        type = "http"
        path = "/health"
        interval = "10s"
        timeout = "3s"
      }

      tags = [
        "traefik.enable=true",
        "traefik.consulcatalog.connect=true",
        "traefik.http.routers.influxdb.entrypoints=https",
        "traefik.http.routers.influxdb.rule=Host(`influxdb.hq.carboncollins.se`)",
        "traefik.http.routers.influxdb.tls=true",
        "traefik.http.routers.influxdb.tls.certresolver=lets-encrypt",
        "traefik.http.routers.influxdb.tls.domains[0].main=*.hq.carboncollins.se"
      ]
    }

    task "influxdb" {  
      driver = "docker"
      leader = true

      volume_mount {
        volume = "influx"
        destination = "/var/lib/influxdb2"
        read_only = false
      }

      resources {
        cpu = 2000
        memory = 3072
      }

      config {
        image = "influxdb:2.0-alpine"

        volumes = [
          "local/config/:/etc/influxdb2/influx-configs"
        ]
      }

      template {
        data = <<EOH
          TZ='Europe/Stockholm'

          {{ with secret "jobs/database-ts/tasks/influxdb/buckets/orion" }}
          DOCKER_INFLUXDB_INIT_ORG={{ index .Data.data "organisation" }}
          DOCKER_INFLUXDB_INIT_BUCKET={{ index .Data.data "name" }}
          {{ end }}

          {{ with secret "jobs/database-ts/tasks/influxdb/users/carbon" }}
          DOCKER_INFLUXDB_INIT_USERNAME={{ index .Data.data "username" }}
          DOCKER_INFLUXDB_INIT_PASSWORD={{ index .Data.data "password" }}
          {{ end }}
        EOH

        destination = "secrets/influxdb.env"
        env = true
      }
    }
  }

  reschedule {
    delay = "10s"
    delay_function = "exponential"
    max_delay = "10m"
    unlimited = true
  }

  update {
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "10m"
    progress_deadline = "15m"
    auto_revert = true
  }

  meta {
    gitSha = "[[ .gitSha ]]"
    gitBranch = "[[ .gitBranch ]]"
    pipelineId = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId = "[[ .projectId ]]"
    projectUrl = "[[ .projectUrl ]]"
    statefull = "true"
  }
}
